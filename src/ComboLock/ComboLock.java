package ComboLock;

import java.util.ArrayList;
import java.util.Scanner;
public class ComboLock {
    ArrayList<Integer> comboArray = new ArrayList<Integer>();
    ArrayList<Boolean> ticksCorrect = new ArrayList<Boolean>();
    int lockIterator;// a value that iterates through the combo lock

    /*
    programming assignment is not really clear about the how the lock works so im going to create an assumption
    for the first number you start from 0 and turn left until the first number
    the second number must be larger than the first number
    the third number
    in example shouldnt the last value be 15 ticks to the right and not 25 to the right
     */

    public ComboLock(int firstComboArg, int secondComboArg, int thirdComboArg) {
        comboArray.add(firstComboArg);
        comboArray.add(secondComboArg);
        comboArray.add(thirdComboArg);
    }
    public void turnLeft(int ticksArg){
        if(ticksCorrect.size() == 1){ticksCorrect.add(false);}// if turnLeft is called on the second combo value, it is false
        if(ticksCorrect.size() > 3){ticksCorrect.add(false);}

        if(ticksCorrect.size() == 0){//if it is the first value
            lockIterator = 40 - ticksArg;
            if(lockIterator == comboArray.get(0).intValue() ){ticksCorrect.add(true);}
            else{ticksCorrect.add(false);}
        }
        if(ticksCorrect.size() == 2){
            if(comboArray.get(1) > comboArray.get(2)){
                int ticksToNext = comboArray.get(2) - comboArray.get(1);
                if(ticksToNext == ticksArg){ticksCorrect.add(true);}
                else{ticksCorrect.add(false);}
            }
            else if(comboArray.get(1) < comboArray.get(2)){
                int ticksToZero = 40 - comboArray.get(2);// the distance between 0 and 15 = 15, dist between 2 and 0 is 2
                int ticksFromZero = comboArray.get(1);
                int totalTicks = ticksFromZero + ticksToZero;
//                System.out.println(totalTicks);
                if(totalTicks == ticksArg){ticksCorrect.add(true);}
                else{ticksCorrect.add(false);}
            }

        }


//        System.out.println(ticksCorrect);
    }
    public void turnRight(int ticksArg){
        if(ticksCorrect.size() == 0){ticksCorrect.add(false);}
        if(ticksCorrect.size() > 3){ticksCorrect.add(false);}
        if(ticksCorrect.size() == 1 ){
            lockIterator = ticksArg + lockIterator;
            if(lockIterator == comboArray.get(1).intValue() ){ticksCorrect.add(true);}
            else{ticksCorrect.add(false);}
        }
//        System.out.println(ticksCorrect);
    }
    public boolean open(){
        if(ticksCorrect.size() != 3){
            System.out.println("There are not enough turns to be able to open the lock");
            return false;
        }
        if (ticksCorrect.contains(false)){
            System.out.println("One of the combinations is not correct");
            return false;
        }
        else{
            System.out.println("Lock has been opened");
            return true;}

    }
    public void reset(){
        ticksCorrect.clear();
    }


    public static void main(String[] args) {


        ComboLock object = new ComboLock(10,15,30);
        object.turnLeft(30);
        object.turnRight(1);
        object.turnLeft(25);
        object.open();
        object.reset();

        object.turnLeft(30);
        object.turnRight(5);
        object.turnLeft(25);
        object.open();



    }
}