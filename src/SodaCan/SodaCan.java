package SodaCan;
import java.util.Scanner;
public class SodaCan {
    public double height;// heights and diameters can be decimal
    public double diameter;
    public double radius; //defininely useful to make a radius instance variable because we are going to use it a lot
    SodaCan(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Soda Can's height first and the Soda Can's diameter second");
        System.out.println("Seperate the values with a space");
        String input = scanner.nextLine();// like to use string then convert to be able to validate
        String [] inputArray = input.split(" ");
        if(inputArray.length != 2){
            System.out.println("Please input only two numbers seperated by space");
        }

        try{
            height = Double.valueOf(inputArray[0]);
        }
        catch(Exception e){
            System.out.println("The height is not a number. Letter characters and other special characters might exist");
        }

        try{
            diameter = Double.valueOf(inputArray[1]);
            radius = diameter / 2;
        }
        catch(Exception e){
            System.out.println("The diameter is not a number. Letter characters and other special characters might exist");
        }

    }//constructor ending bracket
    public double getVolume(){
        // formula http://jwilson.coe.uga.edu/emt725/class/trabue/can/prob1.html
        // v = pi * r^2 * h
//        double radius = diameter / 2;
        double volume = height * Math.pow(radius,2) * Math.PI;
        System.out.print("The volume is ");
        System.out.print(volume);
        return volume;
    }
    public double getSurfaceArea(){
        // formula https://www.reference.com/world-view/surface-soda-can-970750cf7db104b4
        //[2 * pi *r^2] + [pi * d * h]
        double CanTopBot = 2 * Math.PI * Math.pow(radius,2);

        double canSide = Math.PI * diameter * height ;
        double surfaceArea = CanTopBot + canSide;
        System.out.println(" ");//added to seperate the volume and surface area
        System.out.print("The surface area is ");
        System.out.print(surfaceArea);
        return surfaceArea;
    }

    public static void main(String[] args) {
        SodaCan object = new SodaCan();
        object.getVolume();
        object.getSurfaceArea();

    }
}
